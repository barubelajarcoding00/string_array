<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih String</h1>
    <?php
    echo "<h3> Soal No 1</h3>";
    $string = "Hello PHP!";
    $panjangkalimat = strlen($string) ;
    $jumlahkata = str_word_count($string);
    echo "kalimat : $string <br>";
    echo "Panjang String : $panjangkalimat <br>";
    echo "Jumlah Kata : $jumlahkata <br>";

    $string1 = "I'm ready for the challenges";
    $panjangEjaan = strlen($string1) ;
    $jumlahEja = str_word_count($string1);
    echo "mengeja : $string1 <br>";
    echo "Ejaan : $panjangEjaan <br>";
    echo "jumlaheja : $jumlahEja <br>";




    echo "<h2>Soal No 2 </h2>";
    $string2 = "I love PHP";
    echo "kalimat : $string2 <br>";
    echo "Kata Pertama : ". substr($string2, 0, 6). "<br>";
    echo "Kata Kedua :". substr($string2, 6, 5);

    echo "<h3>Soal No 3 </h3>";
    $string3 = "PHP is old but sexy!";
    echo "Kalimat : $string3 <br>";
    echo "Kalimat String Diganti : ". str_replace("sexy!", "awesome",$string3);
    ?>
</body>
</html>